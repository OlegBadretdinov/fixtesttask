//
//  UITableViewCell+Identifier.h
//  Fix
//
//  Created by Oleg Badretdinov on 04.03.2018.
//  Copyright © 2018 Oleg Badretdinov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITableViewCell (Identifier)

+ (NSString *)cellIdentifier;

@end
