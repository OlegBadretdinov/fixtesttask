//
//  UICollectionViewCell+Identifier.h
//  Fix
//
//  Created by Oleg Badretdinov on 10.03.2018.
//  Copyright © 2018 Oleg Badretdinov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UICollectionViewCell (Identifier)

+ (NSString *)cellIdentifier;

@end
