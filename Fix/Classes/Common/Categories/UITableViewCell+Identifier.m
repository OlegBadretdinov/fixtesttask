//
//  UITableViewCell+Identifier.m
//  Fix
//
//  Created by Oleg Badretdinov on 04.03.2018.
//  Copyright © 2018 Oleg Badretdinov. All rights reserved.
//

#import "UITableViewCell+Identifier.h"

@implementation UITableViewCell (Identifier)

+ (NSString *)cellIdentifier {
    return NSStringFromClass(self.self);
}

@end
