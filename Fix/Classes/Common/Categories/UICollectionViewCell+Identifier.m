//
//  UICollectionViewCell+Identifier.m
//  Fix
//
//  Created by Oleg Badretdinov on 10.03.2018.
//  Copyright © 2018 Oleg Badretdinov. All rights reserved.
//

#import "UICollectionViewCell+Identifier.h"

@implementation UICollectionViewCell (Identifier)

+ (NSString *)cellIdentifier {
    return NSStringFromClass(self.self);
}

@end
