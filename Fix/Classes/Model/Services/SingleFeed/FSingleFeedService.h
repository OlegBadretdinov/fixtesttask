//
//  FSingleFeedService.h
//  Fix
//
//  Created by Oleg Badretdinov on 10.03.2018.
//  Copyright © 2018 Oleg Badretdinov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <TyphoonAutoInjection.h>
#import "FFeedNetworkService.h"
#import "FFeedStorage.h"

@protocol FSingleFeedServiceProtocol

- (void)likeFeed:(FPlainFeed *)feed withCompletionBlock:(void (^)(NSNumber *, NSError *))completion;
- (void)removeLike:(FPlainFeed *)feed withCompletionBlock:(void (^)(NSNumber *, NSError *))completion;

@end

@interface FSingleFeedService : NSObject

@property(nonatomic, retain) InjectedProtocol(FFeedStorageProtocol) storage;
@property(nonatomic, retain) InjectedProtocol(FFeedNetworkServiceProtocol) network;

@end
