//
//  FSingleFeedService.m
//  Fix
//
//  Created by Oleg Badretdinov on 10.03.2018.
//  Copyright © 2018 Oleg Badretdinov. All rights reserved.
//

#import "FSingleFeedService.h"
#import <VKSdk.h>

@implementation FSingleFeedService

- (void)likeFeed:(FPlainFeed *)feed withCompletionBlock:(void (^)(NSNumber *, NSError *))completion {
    [self.network likeFeedWithId:feed.identifier ownerId:feed.ownerId completionBlock:^(NSNumber *likes, NSError *error) {
        if (likes) {
            feed.likes.count = likes.unsignedIntegerValue;
            feed.likes.userLikes = true;
            
            [self.storage cacheFeed:@[feed]];
        }
        completion(likes, error);
    }];
}

- (void)removeLike:(FPlainFeed *)feed withCompletionBlock:(void (^)(NSNumber *, NSError *))completion {
    [self.network removeLikeFromFeedWithId:feed.identifier ownerId:feed.ownerId completionBlock:^(NSNumber *likes, NSError *error) {
        if (likes) {
            feed.likes.count = likes.unsignedIntegerValue;
            feed.likes.userLikes = false;
            
            [self.storage cacheFeed:@[feed]];
        }
        completion(likes, error);
    }];
}

@end
