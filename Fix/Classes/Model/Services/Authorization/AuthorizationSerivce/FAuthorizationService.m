//
//  FAuthorizationService.m
//  Fix
//
//  Created by Лена Вышутина on 26.02.2018.
//  Copyright © 2018 Oleg Badretdinov. All rights reserved.
//

#import "FAuthorizationService.h"
#import <VKSdk.h>
#import <libextobjc/extobjc.h>

@interface FAuthorizationService() <VKSdkDelegate>

@property (nonatomic, retain) NSArray<NSString *>* scopes;

@end

@implementation FAuthorizationService
@synthesize delegate;

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self setupVkSdk];
    }
    return self;
}

- (void)setupVkSdk {
    VKSdk *sdkInstance = [VKSdk initializeWithAppId:@"6387631"];
    [sdkInstance registerDelegate:self];
    
    self.scopes = @[@"wall"];
}

- (void)startAuthorization {
    [VKSdk authorize:self.scopes];
}

- (void)checkAuthorizationStatus {
    @weakify(self);
    [VKSdk wakeUpSession:self.scopes completeBlock:^(VKAuthorizationState state, NSError *error) {
        @strongify(self);
        [self handleStatus:state];
    }];
}

- (void)vkSdkAccessAuthorizationFinishedWithResult:(VKAuthorizationResult *)result {
    [self handleStatus:result.state];
}

- (void)vkSdkUserAuthorizationFailed {
    if (self.delegate && [self.delegate respondsToSelector:@selector(authDidChangeStatus:)]) {
        [self.delegate authDidChangeStatus:FAuthorizationStatusError];
    }
}

- (void)handleStatus:(VKAuthorizationState)status {
    FAuthorizationStatus localStatus = FAuthorizationStatusUnknown;
    
    switch (status) {
        case VKAuthorizationUnknown:
            localStatus = FAuthorizationStatusUnknown;
            break;
        case VKAuthorizationInitialized:
            localStatus = FAuthorizationStatusUnknown;
            break;
        case VKAuthorizationError:
            localStatus = FAuthorizationStatusError;
            break;
        case VKAuthorizationAuthorized:
            localStatus = FAuthorizationStatusAuthorized;
            break;
        default:
            localStatus = FAuthorizationStatusWaiting;
            break;
    }
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(authDidChangeStatus:)]) {
        [self.delegate authDidChangeStatus:localStatus];
    }
}

- (void)logOut {
    [VKSdk forceLogout];
    if (self.delegate && [self.delegate respondsToSelector:@selector(authDidChangeStatus:)]) {
        [self.delegate authDidChangeStatus:FAuthorizationStatusUnknown];
    }
}

@end
