//
//  FAuthorizationService.h
//  Fix
//
//  Created by Лена Вышутина on 26.02.2018.
//  Copyright © 2018 Oleg Badretdinov. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, FAuthorizationStatus) {
    FAuthorizationStatusUnknown,
    FAuthorizationStatusWaiting,
    FAuthorizationStatusError,
    FAuthorizationStatusAuthorized
};

@protocol FAuthorizationServiceDelegate <NSObject>

@optional
- (void)authDidChangeStatus:(FAuthorizationStatus )status;

@end

@protocol FAuthorizationServiceProtocol

@property (nonatomic, weak) id<FAuthorizationServiceDelegate> delegate;

- (void)startAuthorization;
- (void)checkAuthorizationStatus;
- (void)logOut;
    
@end

@interface FAuthorizationService : NSObject <FAuthorizationServiceProtocol>

@end
