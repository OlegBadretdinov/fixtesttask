//
//  FAuthorizationTimerService.m
//  Fix
//
//  Created by Oleg Badretdinov on 27.02.2018.
//  Copyright © 2018 Oleg Badretdinov. All rights reserved.
//

#import "FAuthorizationTimerService.h"

@interface FAuthorizationTimerService()

@property (nonatomic, retain) NSTimer* timer;

@end

@implementation FAuthorizationTimerService
@synthesize delegate;

- (void)startTimer {
    if (!self.timer) {
        self.timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(handleTimer) userInfo:nil repeats:true];
    }
}

- (void)stopTimer {
    if (self.timer) {
        [self.timer invalidate];
        self.timer = nil;
    }
}

- (void)handleTimer {
    if (self.delegate && [self.delegate respondsToSelector:@selector(shouldCheckAuthorizationStatus)]) {
        [self.delegate shouldCheckAuthorizationStatus];
    }
}

@end
