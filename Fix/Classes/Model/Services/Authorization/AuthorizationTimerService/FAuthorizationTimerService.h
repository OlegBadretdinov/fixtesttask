//
//  FAuthorizationTimerService.h
//  Fix
//
//  Created by Oleg Badretdinov on 27.02.2018.
//  Copyright © 2018 Oleg Badretdinov. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol FAuthorizationTimerServiceDelegate <NSObject>
@optional
- (void)shouldCheckAuthorizationStatus;

@end

@protocol FAuthorizationTimerServiceProtocol <NSObject>

@property (nonatomic, weak) id<FAuthorizationTimerServiceDelegate> delegate;

- (void)startTimer;
- (void)stopTimer;

@end

@interface FAuthorizationTimerService : NSObject <FAuthorizationTimerServiceProtocol>

@end
