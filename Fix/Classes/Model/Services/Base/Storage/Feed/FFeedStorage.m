//
//  FFeedStorage.m
//  Fix
//
//  Created by Oleg Badretdinov on 28.02.2018.
//  Copyright © 2018 Oleg Badretdinov. All rights reserved.
//

#import "FFeedStorage.h"
#import "PlainManagedObjectsExtensions.h"
#import "FPlainFeed.h"

@implementation FFeedStorage

- (void)getFeedWithOffset:(NSNumber *)offset count:(NSNumber *)count completionBlock:(void (^)(NSArray<FPlainFeed *> *))completion {
    NSArray *objects = [self.storageService fetchObjectsWithOffset:offset count:count withClass:[FPlainFeed class]];
    completion(objects);
}

- (void)cacheFeed:(NSArray<FPlainFeed *> *)feed {
    [self.storageService saveObjects:feed];
}

- (void)clearFeed {
    [self.storageService dropObjectsWithClass:[FPlainFeed class]];
}

@end
