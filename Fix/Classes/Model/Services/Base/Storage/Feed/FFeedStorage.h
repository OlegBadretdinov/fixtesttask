//
//  FFeedStorage.h
//  Fix
//
//  Created by Oleg Badretdinov on 28.02.2018.
//  Copyright © 2018 Oleg Badretdinov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FPlainFeed.h"
#import "CoreDataStorageService.h"

@protocol FFeedStorageProtocol <NSObject>

-(void)getFeedWithOffset:(NSNumber *)offset count:(NSNumber *)count completionBlock:(void (^)(NSArray<FPlainFeed *> *))completion;

-(void)cacheFeed:(NSArray<FPlainFeed *> *)feed;

-(void)clearFeed;

@end

@interface FFeedStorage : NSObject <FFeedStorageProtocol>

@property (nonatomic, retain) id<CoreDataStorageServiceProtocol> storageService;

@end
