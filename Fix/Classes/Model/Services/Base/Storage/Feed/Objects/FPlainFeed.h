//
//  FPlainFeed.h
//  Fix
//
//  Created by Oleg Badretdinov on 28.02.2018.
//  Copyright © 2018 Oleg Badretdinov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Mantle.h>

@interface FComments :MTLModel <MTLJSONSerializing>

@property (nonatomic, assign) NSInteger count;

@end

@interface FLikes : MTLModel <MTLJSONSerializing>

@property (nonatomic, assign) BOOL userLikes;
@property (nonatomic, assign) NSInteger count;

@end

@interface FReposts : MTLModel <MTLJSONSerializing>

@property (nonatomic, assign) NSInteger count;

@end

@interface FPhoto : MTLModel <MTLJSONSerializing>

@property (nonatomic, assign) NSUInteger identifier;

@property (nonatomic, retain) NSURL *photo1280;
@property (nonatomic, retain) NSURL *photo130;
@property (nonatomic, retain) NSURL *photo604;
@property (nonatomic, retain) NSURL *photo75;
@property (nonatomic, retain) NSURL *photo807;

@end

@interface FAttachments : MTLModel <MTLJSONSerializing>

@property (nonatomic, retain) NSString *type;
@property (nonatomic, retain) FPhoto *photo;

@end

@interface FPlainFeed : MTLModel <MTLJSONSerializing>

@property (nonatomic, assign) NSUInteger identifier;
@property (nonatomic, assign) NSInteger ownerId;
@property (nonatomic, assign) NSInteger fromId;
@property (nonatomic, assign) NSInteger createdBy;
@property (nonatomic, retain) NSDate *date;
@property (nonatomic, retain) NSString *text;
@property (nonatomic, retain) FComments *comments;
@property (nonatomic, retain) FLikes *likes;
@property (nonatomic, retain) FReposts *reposts;
@property (nonatomic, retain) NSArray<FAttachments*> *attachements;

@end
