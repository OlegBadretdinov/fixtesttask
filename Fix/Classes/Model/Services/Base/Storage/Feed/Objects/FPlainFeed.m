//
//  FPlainFeed.m
//  Fix
//
//  Created by Oleg Badretdinov on 28.02.2018.
//  Copyright © 2018 Oleg Badretdinov. All rights reserved.
//

#import "FPlainFeed.h"

@implementation FComments

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{@"count": @"count"};
}

@end

@implementation FLikes

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{@"count": @"count",
             @"userLikes" : @"user_likes"
             };
}

+ (NSValueTransformer *)userLikesJSONTransformer {
    return [NSValueTransformer valueTransformerForName:MTLBooleanValueTransformerName];
}

@end

@implementation FReposts

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{@"count": @"count"};
}

@end

@implementation FPhoto

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{@"identifier" : @"id",
             @"photo1280" : @"photo_1280",
             @"photo130" : @"photo_130",
             @"photo604" : @"photo_604",
             @"photo75" : @"photo_75",
             @"photo807" : @"photo_807"
             };
}

+ (NSValueTransformer *)photo1280JSONTransformer {
    return [NSValueTransformer valueTransformerForName:MTLURLValueTransformerName];
}
+ (NSValueTransformer *)photo130JSONTransformer {
    return [NSValueTransformer valueTransformerForName:MTLURLValueTransformerName];
}
+ (NSValueTransformer *)photo604JSONTransformer {
    return [NSValueTransformer valueTransformerForName:MTLURLValueTransformerName];
}
+ (NSValueTransformer *)photo75JSONTransformer {
    return [NSValueTransformer valueTransformerForName:MTLURLValueTransformerName];
}
+ (NSValueTransformer *)photo807JSONTransformer {
    return [NSValueTransformer valueTransformerForName:MTLURLValueTransformerName];
}

@end

@implementation FAttachments

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{@"type" : @"type",
             @"photo" : @"photo"
             };
}

+ (NSValueTransformer *)photoJSONTransformer {
    return [MTLJSONAdapter dictionaryTransformerWithModelClass:FPhoto.class];
}

@end

@implementation FPlainFeed

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{@"identifier": @"id",
             @"ownerId": @"owner_id",
             @"fromId": @"from_id",
             @"createdBy": @"created_by",
             @"date": @"date",
             @"text": @"text",
             @"comments": @"comments",
             @"likes": @"likes",
             @"reposts": @"reposts",
             @"attachements": @"attachments"
             };
}

+ (NSValueTransformer *)commentsJSONTransformer {
    return [MTLJSONAdapter dictionaryTransformerWithModelClass:FComments.class];
}
+ (NSValueTransformer *)likesJSONTransformer {
    return [MTLJSONAdapter dictionaryTransformerWithModelClass:FLikes.class];
}
+ (NSValueTransformer *)repostsJSONTransformer {
    return [MTLJSONAdapter dictionaryTransformerWithModelClass:FReposts.class];
}
+ (NSValueTransformer *)attachementsJSONTransformer {
    return [MTLJSONAdapter arrayTransformerWithModelClass:FAttachments.class];
}
+ (NSValueTransformer *)dateJSONTransformer {
    return [MTLValueTransformer transformerUsingForwardBlock:^id(NSNumber *value, BOOL *success, NSError *__autoreleasing *error) {
        return [NSDate dateWithTimeIntervalSince1970:value.doubleValue];
    } reverseBlock:^id(NSDate *value, BOOL *success, NSError *__autoreleasing *error) {
        return @([[NSDate date] timeIntervalSince1970]);
    }];
}

@end
