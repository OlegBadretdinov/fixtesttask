//
//  CoreDataStorageService.h
//  Fix
//
//  Created by Oleg Badretdinov on 28.02.2018.
//  Copyright © 2018 Oleg Badretdinov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import <Mantle.h>

@protocol CoreDataStorageServiceProtocol <NSObject>

- (NSArray *)fetchObjectsWithOffset:(NSNumber *)offset count:(NSNumber *)count  withClass:(Class)plainClass;
- (void)saveObjects:(NSArray *)objects;
- (void)dropObjectsWithClass:(Class)plainClass;

@end

@interface CoreDataStorageService<ObjectType>: NSObject <CoreDataStorageServiceProtocol>

@end
