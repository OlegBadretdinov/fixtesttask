//
//  CoreDataStorageService.m
//  Fix
//
//  Created by Oleg Badretdinov on 28.02.2018.
//  Copyright © 2018 Oleg Badretdinov. All rights reserved.
//

#import "CoreDataStorageService.h"
#import "PlainManagedObjectsExtensions.h"
#import <UIKit/UIKit.h>

@interface CoreDataStorageService()

@property (readonly, strong, nonatomic) NSManagedObjectContext *privateContext;
@property (readonly, strong, nonatomic) NSManagedObjectContext *mainContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

@end

@implementation CoreDataStorageService

@synthesize mainContext = _mainContext;
@synthesize privateContext = _privateContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

- (NSURL *)cacheDirectory {
    return [[[NSFileManager defaultManager] URLsForDirectory:NSCachesDirectory inDomains:NSUserDomainMask] lastObject];
}

- (NSManagedObjectModel *)managedObjectModel {
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"Fix" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    // The persistent store coordinator for the application. This implementation creates and returns a coordinator, having added the store for the application to it.
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    // Create the coordinator and store
    
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    NSURL *storeURL = [[self cacheDirectory] URLByAppendingPathComponent:@"Cache.sqlite"];
    NSError *error = nil;
    NSString *failureReason = @"There was an error creating or loading the application's saved data.";
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        // Report any error we got.
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        dict[NSLocalizedDescriptionKey] = @"Failed to initialize the application's saved data";
        dict[NSLocalizedFailureReasonErrorKey] = failureReason;
        dict[NSUnderlyingErrorKey] = error;
        error = [NSError errorWithDomain:@"YOUR_ERROR_DOMAIN" code:9999 userInfo:dict];
        // Replace this with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _persistentStoreCoordinator;
}

- (NSManagedObjectContext *)privateContext {
    if (_privateContext != nil) {
        return _privateContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (!coordinator) {
        return nil;
    }
    _privateContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
    [_privateContext setPersistentStoreCoordinator:coordinator];
    return _privateContext;
}

- (NSManagedObjectContext *)mainContext {
    if (_mainContext != nil) {
        return _mainContext;
    }
    
    _mainContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
    [_mainContext setParentContext:[self privateContext]];
    return _mainContext;
}

- (void)saveContext {
    if ([self.mainContext hasChanges]) {
        [self.mainContext save:nil];
    }
    if ([self.privateContext hasChanges]) {
        [self.privateContext save:nil];
    }
}

- (NSArray *)fetchObjectsWithOffset:(NSNumber *)offset count:(NSNumber *)count withClass:(Class)plainClass {
    if ([plainClass isSubclassOfClass:[MTLModel class]]) {
        NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:NSStringFromClass([plainClass managedClass])];
        request.fetchOffset = offset.unsignedIntegerValue;
        request.fetchLimit = count.unsignedIntegerValue;
        NSSortDescriptor *identifierDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"identifier" ascending:NO];
        request.sortDescriptors = @[identifierDescriptor];
        NSArray *managedObjects = [self.mainContext executeFetchRequest:request error:nil];
        NSMutableArray *plainObjects = [NSMutableArray new];
        for (NSManagedObject *managedObject in managedObjects) {
            [plainObjects addObject:[managedObject plainObject]];
        }
        return plainObjects;
    } else {
        @throw [NSException exceptionWithName:@"Wrong class" reason:@"Wrong class" userInfo:nil];
    }
}

- (void)saveObjects:(NSArray<MTLModel *> *)objects {
    if (objects.count > 0) {
        NSManagedObjectContext *newContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
        [newContext setParentContext:[self mainContext]];
        
        //TODO: Сделать нормальное обновление, а не удалить/добавить
        NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:NSStringFromClass([[[objects firstObject] class] managedClass])];
        NSArray *ids = [objects valueForKey:@"identifier"];
        request.predicate = [NSPredicate predicateWithFormat:@"identifier in %@", ids];
        NSArray *managedObjects = [newContext executeFetchRequest:request error:nil];
        for (NSManagedObject *managedObject in managedObjects) {
            [newContext deleteObject:managedObject];
        }
        
        for (MTLModel* singleObject in objects) {
            [singleObject managedObjectInContext:newContext];
        }
        [newContext save:nil];
        [self saveContext];
    }
}

- (void)dropObjectsWithClass:(Class)plainClass {
    NSManagedObjectContext *newContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
    [newContext setParentContext:[self mainContext]];
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:NSStringFromClass([plainClass managedClass])];
    NSArray *managedObjects = [newContext executeFetchRequest:request error:nil];
    for (NSManagedObject *managedObject in managedObjects) {
        [newContext deleteObject:managedObject];
    }
    [newContext save:nil];
    [self saveContext];
}

@end
