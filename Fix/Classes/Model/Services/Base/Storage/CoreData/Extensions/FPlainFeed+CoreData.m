//
//  FPlainFeed+CoreData.m
//  Fix
//
//  Created by Oleg Badretdinov on 06.03.2018.
//  Copyright © 2018 Oleg Badretdinov. All rights reserved.
//

#import "PlainManagedObjectsExtensions.h"
#import "FPlainFeed.h"
#import "CFeed+CoreDataClass.h"
#import "CComments+CoreDataClass.h"
#import "CLikes+CoreDataClass.h"
#import "CReposts+CoreDataClass.h"
#import "CPhoto+CoreDataClass.h"


//TODO: - Посмотреть, может можно будет оптимизировать через objc runtime
@implementation FPlainFeed (CoreData)

+ (Class)managedClass {
    return [CFeed class];
}

- (NSManagedObject *)managedObjectInContext:(NSManagedObjectContext *)context {
    CFeed *feed = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([CFeed class]) inManagedObjectContext:context];
    feed.identifier = (int64_t)self.identifier;
    feed.ownerId = (int64_t)self.ownerId;
    feed.fromId = (int64_t)self.fromId;
    feed.createdBy = (int64_t)self.createdBy;
    feed.date = self.date;
    feed.text = self.text;
    
    feed.likes = (CLikes *)[self.likes managedObjectInContext:context];
    feed.comments = (CComments *)[self.comments managedObjectInContext:context];
    feed.reposts = (CReposts *)[self.reposts managedObjectInContext:context];
    
    NSMutableSet *photos = [NSMutableSet new];
    for (FAttachments *attachment in self.attachements) {
        [photos addObject:[attachment.photo managedObjectInContext:context]];
    }
    feed.attachements = photos;
    
    return feed;
}

@end

@implementation FLikes (CoreData)
- (NSManagedObject *)managedObjectInContext:(NSManagedObjectContext *)context {
    CLikes *like = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([CLikes class]) inManagedObjectContext:context];
    like.count = (int32_t)self.count;
    like.userLikes = self.userLikes;
    return like;
}
@end

@implementation FReposts (CoreData)
- (NSManagedObject *)managedObjectInContext:(NSManagedObjectContext *)context {
    CReposts *repost = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([CReposts class]) inManagedObjectContext:context];
    repost.count = (int32_t)self.count;
    return repost;
}
@end

@implementation FComments (CoreData)
- (NSManagedObject *)managedObjectInContext:(NSManagedObjectContext *)context {
    CComments *comments = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([CComments class]) inManagedObjectContext:context];
    comments.count = (int32_t)self.count;
    return comments;
}
@end

@implementation FPhoto (CoreData)
- (NSManagedObject *)managedObjectInContext:(NSManagedObjectContext *)context {
    CPhoto *photo = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([CPhoto class]) inManagedObjectContext:context];
    photo.identifier = (int64_t)self.identifier;
    photo.photo75 = self.photo75.absoluteString;
    photo.photo130 = self.photo130.absoluteString;
    photo.photo604 = self.photo604.absoluteString;
    photo.photo807 = self.photo807.absoluteString;
    photo.photo1280 = self.photo1280.absoluteString;
    return photo;
}
@end

@implementation CFeed (Mantle)

+ (Class)plainClass {
    return [FPlainFeed class];
}

- (MTLModel *)plainObject {
    FPlainFeed *feed = [FPlainFeed new];
    feed.identifier = self.identifier;
    feed.ownerId = self.ownerId;
    feed.fromId = self.fromId;
    feed.createdBy = self.createdBy;
    feed.date = self.date;
    feed.text = self.text;
    
    feed.comments = (FComments *)[self.comments plainObject];
    
    feed.likes = (FLikes *)[self.likes plainObject];
    
    feed.reposts = (FReposts *)[self.reposts plainObject];
    
    NSMutableArray *attachements = [NSMutableArray new];
    for (CPhoto *photo in self.attachements) {
        FAttachments *newAttachment = [FAttachments new];
        newAttachment.photo = (FPhoto *)[photo plainObject];
        [attachements addObject:newAttachment];
    }
    feed.attachements = attachements;
    return feed;
}

@end

@implementation CComments (Mantle)

- (MTLModel *)plainObject {
    FComments *comments = [FComments new];
    comments.count = self.count;
    return comments;
}

@end

@implementation CLikes (Mantle)

- (MTLModel *)plainObject {
    FLikes *likes = [FLikes new];
    likes.userLikes = self.userLikes;
    likes.count = self.count;
    return likes;
}

@end

@implementation CReposts (Mantle)

- (MTLModel *)plainObject {
    FReposts *reposts = [FReposts new];
    reposts.count = self.count;
    return reposts;
}

@end

@implementation CPhoto (Mantle)

- (MTLModel *)plainObject {
    FPhoto *photo = [FPhoto new];
    photo.identifier = self.identifier;
    photo.photo75 = [NSURL URLWithString:self.photo75];
    photo.photo130 = [NSURL URLWithString:self.photo130];
    photo.photo604 = [NSURL URLWithString:self.photo604];
    photo.photo807 = [NSURL URLWithString:self.photo807];
    photo.photo1280 = [NSURL URLWithString:self.photo1280];
    return photo;
}

@end

