//
//  PlainManagedObjectsExtensions.h
//  Fix
//
//  Created by Oleg Badretdinov on 06.03.2018.
//  Copyright © 2018 Oleg Badretdinov. All rights reserved.
//

#import <CoreData/CoreData.h>
#import <Mantle.h>

@interface NSManagedObject (Mantle)

- (MTLModel *)plainObject;
+ (Class)plainClass;

@end

@interface MTLModel (CoreData)

- (NSManagedObject *)managedObjectInContext:(NSManagedObjectContext *)context;
+ (Class)managedClass;

@end
