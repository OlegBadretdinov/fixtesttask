//
//  FFeedNetworkService.m
//  Fix
//
//  Created by Oleg Badretdinov on 28.02.2018.
//  Copyright © 2018 Oleg Badretdinov. All rights reserved.
//

#import "FFeedNetworkService.h"
#import <VKApi.h>
#import <Mantle.h>

static NSString *const WALL_GET = @"wall.get";
static NSString *const LIKE_ADD = @"likes.add";
static NSString *const LIKE_DELETE = @"likes.delete";


@implementation FFeedNetworkService

- (void)fetchFeedsWithOffset:(NSNumber *)offset count:(NSNumber *)count completionBlock:(void (^)(NSArray<FPlainFeed *>*, NSError *))completion {
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:@"-80174024", VK_API_OWNER_ID, nil];
    if (offset) {
        params[VK_API_OFFSET] = offset;
    }
    if (count) {
        params[VK_API_COUNT] = count;
    }
    VKRequest *request = [VKRequest requestWithMethod:WALL_GET parameters:params];
    [request executeWithResultBlock:^(VKResponse *response) {
        NSDictionary *responesDict = response.json;
        NSError *error;
        NSArray<FPlainFeed *>* objects = [MTLJSONAdapter modelsOfClass:[FPlainFeed class] fromJSONArray:responesDict[@"items"] error:&error];
        for (FPlainFeed* feedObject in objects) {
            NSMutableArray *attachments = [NSMutableArray new];
            for (FAttachments *attachement in feedObject.attachements) {
                if ([attachement.type  isEqual: @"photo"]) {
                    [attachments addObject:attachement];
                }
            }
            feedObject.attachements = [attachments copy];
        }
        completion(objects, error);
    } errorBlock:^(NSError *error) {
        completion(nil, error);
    }];
}

- (void)likeFeedWithId:(NSUInteger)postId ownerId:(NSUInteger)ownerId completionBlock:(void (^)(NSNumber *, NSError *))completion {
    NSString *post = [NSString stringWithFormat:@"%lu", (unsigned long)postId];
    NSString *owner = [NSString stringWithFormat:@"%lu", (unsigned long)ownerId];
    VKRequest *request = [VKRequest requestWithMethod:LIKE_ADD parameters:@{@"type" : @"post", @"item_id" : post, @"owner_id" : owner}];
    [request executeWithResultBlock:^(VKResponse *response) {
        NSDictionary *resp = response.json;
        NSNumber *likes = resp[@"likes"];
        completion(likes, nil);
    } errorBlock:^(NSError *error) {
        completion(nil, error);
    }];
}

- (void)removeLikeFromFeedWithId:(NSUInteger)postId ownerId:(NSUInteger)ownerId completionBlock:(void (^)(NSNumber *, NSError *))completion {
    NSString *post = [NSString stringWithFormat:@"%lu", (unsigned long)postId];
    NSString *owner = [NSString stringWithFormat:@"%lu", (unsigned long)ownerId];
    VKRequest *request = [VKRequest requestWithMethod:LIKE_DELETE parameters:@{@"type" : @"post", @"item_id" : post, @"owner_id" : owner}];
    [request executeWithResultBlock:^(VKResponse *response) {
        NSDictionary *resp = response.json;
        NSNumber *likes = resp[@"likes"];
        completion(likes, nil);
    } errorBlock:^(NSError *error) {
        completion(nil, error);
    }];
}

@end
