//
//  FFeedNetworkService.h
//  Fix
//
//  Created by Oleg Badretdinov on 28.02.2018.
//  Copyright © 2018 Oleg Badretdinov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FPlainFeed.h"

@protocol FFeedNetworkServiceProtocol <NSObject>

-(void)fetchFeedsWithOffset:(NSNumber *)offset count:(NSNumber *)count completionBlock:(void (^)(NSArray<FPlainFeed *>*, NSError *))completion;
- (void)removeLikeFromFeedWithId:(NSUInteger)postId ownerId:(NSUInteger)ownerId completionBlock:(void (^)(NSNumber *, NSError *))completion;
- (void)likeFeedWithId:(NSUInteger)postId ownerId:(NSUInteger)ownerId completionBlock:(void (^)(NSNumber *, NSError *))completion;

@end

@interface FFeedNetworkService : NSObject <FFeedNetworkServiceProtocol>

@end
