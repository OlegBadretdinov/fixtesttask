//
//  FFeedService.h
//  Fix
//
//  Created by Oleg Badretdinov on 28.02.2018.
//  Copyright © 2018 Oleg Badretdinov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FFeedStorage.h"
#import "FFeedNetworkService.h"
#import <TyphoonAutoInjection.h>
#import "FFeedStorage.h"
#import "FFeedNetworkService.h"

@protocol FFeedServiceProtocol <NSObject>

- (void)getFeedWithOffset:(NSNumber *)offset count:(NSNumber *)count completionBlock:(void (^)(NSArray<FPlainFeed *>*, NSError *))completion;
- (void)clearFeedCacheWithCompletionBlock:(void (^)(void))completion;

@end

@interface FFeedService : NSObject <FFeedServiceProtocol>

@property(nonatomic, retain) InjectedProtocol(FFeedStorageProtocol) storage;
@property(nonatomic, retain) InjectedProtocol(FFeedNetworkServiceProtocol) network;

@end
