//
//  FFeedService.m
//  Fix
//
//  Created by Oleg Badretdinov on 28.02.2018.
//  Copyright © 2018 Oleg Badretdinov. All rights reserved.
//

#import "FFeedService.h"
#import <extobjc.h>

@implementation FFeedService

//TODO: По хорошему обернуть эти 2 метода в Background, тем более что уже все держится на коллбеках
- (void)getFeedWithOffset:(NSNumber *)offset count:(NSNumber *)count completionBlock:(void (^)(NSArray<FPlainFeed *> *, NSError *))completion {
    @weakify(self);
    [self.storage getFeedWithOffset:offset count:count completionBlock:^(NSArray<FPlainFeed *> *feed) {
        @strongify(self);
        if (feed.count == 0) {
            [self fetchFeedFromServerAndCacheLocally:offset count:count completionBlock:completion];
        } else {
            completion(feed, nil);
        }
    }];
    
}

- (void)fetchFeedFromServerAndCacheLocally:(NSNumber *)offset count:(NSNumber *)count completionBlock:(void (^)(NSArray<FPlainFeed *> *, NSError *))completion {
    @weakify(self);
    [self.network fetchFeedsWithOffset:offset count:count completionBlock:^(NSArray<FPlainFeed *> *feeds, NSError *error) {
        @strongify(self);
        if (feeds) {
            [self.storage cacheFeed:feeds];
            completion(feeds, nil);
        } else {
            completion(nil, error);
        }
    }];
}

- (void)clearFeedCacheWithCompletionBlock:(void (^)(void))completion {
    [self.storage clearFeed];
    if (completion) {
        completion();
    }
}

@end
