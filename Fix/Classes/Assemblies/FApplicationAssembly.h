//
//  FApplicationAssembly.h
//  Fix
//
//  Created by Лена Вышутина on 26.02.2018.
//  Copyright © 2018 Oleg Badretdinov. All rights reserved.
//

#import <Typhoon/Typhoon.h>
#import <RamblerTyphoonUtils/AssemblyCollector.h>
#import "CoreDataStorageService.h"
#import "FFeedNetworkService.h"
#import "FFeedStorage.h"

@interface FApplicationAssembly : TyphoonAssembly <RamblerInitialAssembly>

- (FFeedNetworkService *)feedNetwork;
- (FFeedStorage *)feedStorage;

@end
