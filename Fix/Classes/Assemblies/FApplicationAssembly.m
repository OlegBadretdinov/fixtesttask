//
//  FApplicationAssembly.m
//  Fix
//
//  Created by Лена Вышутина on 26.02.2018.
//  Copyright © 2018 Oleg Badretdinov. All rights reserved.
//

#import "FApplicationAssembly.h"

@implementation FApplicationAssembly

- (CoreDataStorageService *)coreDataStorageService {
    return [TyphoonDefinition withClass:[CoreDataStorageService class]];
}

- (FFeedNetworkService *)feedNetwork {
    return [TyphoonDefinition withClass:[FFeedNetworkService class]];
}

- (FFeedStorage *)feedStorage {
    return [TyphoonDefinition withClass:[FFeedStorage class] configuration:^(TyphoonDefinition *definition) {
        [definition injectProperty:@selector(storageService) with:[self coreDataStorageService]];
    }];
}

@end
