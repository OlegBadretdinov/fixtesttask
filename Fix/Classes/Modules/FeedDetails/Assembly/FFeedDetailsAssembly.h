//
//  FFeedDetailsAssembly.h
//  Fix
//
//  Created by Oleg Badretdinov on 09/03/2018.
//  Copyright © 2018 Fix. All rights reserved.
//

#import <Typhoon/Typhoon.h>
#import <RamblerTyphoonUtils/AssemblyCollector.h>

/**
 @author Oleg Badretdinov

 FeedDetails module
 */
@interface FFeedDetailsAssembly : TyphoonAssembly <RamblerInitialAssembly>

@end
