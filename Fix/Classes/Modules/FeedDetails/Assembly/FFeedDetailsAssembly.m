//
//  FFeedDetailsAssembly.m
//  Fix
//
//  Created by Oleg Badretdinov on 09/03/2018.
//  Copyright © 2018 Fix. All rights reserved.
//

#import "FFeedDetailsAssembly.h"
#import "FSingleFeedService.h"

#import "FFeedDetailsViewController.h"
#import "FFeedDetailsInteractor.h"
#import "FFeedDetailsPresenter.h"
#import "FFeedDetailsRouter.h"

#import <ViperMcFlurry/ViperMcFlurry.h>

@implementation FFeedDetailsAssembly

- (FSingleFeedService *)singleFeedService {
    return [TyphoonDefinition withClass:[FSingleFeedService class]];
}

- (FFeedDetailsViewController *)viewFeedDetails {
    return [TyphoonDefinition withClass:[FFeedDetailsViewController class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterFeedDetails]];
                              [definition injectProperty:@selector(moduleInput)
                                                    with:[self presenterFeedDetails]];
                          }];
}

- (FFeedDetailsInteractor *)interactorFeedDetails {
    return [TyphoonDefinition withClass:[FFeedDetailsInteractor class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterFeedDetails]];
                              [definition injectProperty:@selector(feedService) with:[self singleFeedService]];
                          }];
}

- (FFeedDetailsPresenter *)presenterFeedDetails{
    return [TyphoonDefinition withClass:[FFeedDetailsPresenter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(view)
                                                    with:[self viewFeedDetails]];
                              [definition injectProperty:@selector(interactor)
                                                    with:[self interactorFeedDetails]];
                              [definition injectProperty:@selector(router)
                                                    with:[self routerFeedDetails]];
                          }];
}

- (FFeedDetailsRouter *)routerFeedDetails{
    return [TyphoonDefinition withClass:[FFeedDetailsRouter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(transitionHandler)
                                                    with:[self viewFeedDetails]];
                          }];
}

@end
