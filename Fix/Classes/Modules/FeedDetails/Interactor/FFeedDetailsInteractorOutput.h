//
//  FFeedDetailsInteractorOutput.h
//  Fix
//
//  Created by Oleg Badretdinov on 09/03/2018.
//  Copyright © 2018 Fix. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol FFeedDetailsInteractorOutput <NSObject>

- (void)updateLikeCount:(NSNumber *)like;
- (void)setLike:(BOOL)isLiked;
- (void)handleError:(NSError *)error;

@end
