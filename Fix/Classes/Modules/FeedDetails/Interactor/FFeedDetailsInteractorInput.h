//
//  FFeedDetailsInteractorInput.h
//  Fix
//
//  Created by Oleg Badretdinov on 09/03/2018.
//  Copyright © 2018 Fix. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FPlainFeed.h"

@protocol FFeedDetailsInteractorInput <NSObject>

- (void)likeFeed:(FPlainFeed *)feed;
- (void)removeLikeFromFeed:(FPlainFeed *)feed;

@end
