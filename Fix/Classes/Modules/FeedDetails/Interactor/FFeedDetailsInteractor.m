//
//  FFeedDetailsInteractor.m
//  Fix
//
//  Created by Oleg Badretdinov on 09/03/2018.
//  Copyright © 2018 Fix. All rights reserved.
//

#import "FFeedDetailsInteractor.h"
#import "FPlainFeed.h"

#import "FFeedDetailsInteractorOutput.h"

@implementation FFeedDetailsInteractor

#pragma mark - Методы FFeedDetailsInteractorInput

- (void)likeFeed:(FPlainFeed *)feed {
    [self.feedService likeFeed:feed withCompletionBlock:^(NSNumber * likeCount, NSError *error) {
        if (likeCount) {
            [self.output setLike:YES];
            [self.output updateLikeCount:likeCount];
        } else {
            [self.output handleError:error];
        }
    }];
}

- (void)removeLikeFromFeed:(FPlainFeed *)feed {
    [self.feedService removeLike:feed withCompletionBlock:^(NSNumber *likeCount, NSError *error) {
        if (likeCount) {
            [self.output setLike:NO];
            [self.output updateLikeCount:likeCount];
        } else {
            [self.output handleError:error];
        }
    }];
}

@end
