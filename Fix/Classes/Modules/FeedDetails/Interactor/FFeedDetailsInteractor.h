//
//  FFeedDetailsInteractor.h
//  Fix
//
//  Created by Oleg Badretdinov on 09/03/2018.
//  Copyright © 2018 Fix. All rights reserved.
//

#import "FFeedDetailsInteractorInput.h"
#import "FSingleFeedService.h"

@protocol FFeedDetailsInteractorOutput;

@interface FFeedDetailsInteractor : NSObject <FFeedDetailsInteractorInput>

@property (nonatomic, weak) id<FFeedDetailsInteractorOutput> output;
@property (nonatomic, retain) id<FSingleFeedServiceProtocol> feedService;

@end
