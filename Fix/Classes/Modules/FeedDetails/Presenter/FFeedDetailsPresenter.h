//
//  FFeedDetailsPresenter.h
//  Fix
//
//  Created by Oleg Badretdinov on 09/03/2018.
//  Copyright © 2018 Fix. All rights reserved.
//

#import "FFeedDetailsViewOutput.h"
#import "FFeedDetailsInteractorOutput.h"
#import "FFeedDetailsModuleInput.h"

@protocol FFeedDetailsViewInput;
@protocol FFeedDetailsInteractorInput;
@protocol FFeedDetailsRouterInput;

@interface FFeedDetailsPresenter : NSObject <FFeedDetailsModuleInput, FFeedDetailsViewOutput, FFeedDetailsInteractorOutput>

@property (nonatomic, weak) id<FFeedDetailsViewInput> view;
@property (nonatomic, strong) id<FFeedDetailsInteractorInput> interactor;
@property (nonatomic, strong) id<FFeedDetailsRouterInput> router;

@end
