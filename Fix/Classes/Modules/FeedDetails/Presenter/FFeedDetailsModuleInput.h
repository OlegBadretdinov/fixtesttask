//
//  FFeedDetailsModuleInput.h
//  Fix
//
//  Created by Oleg Badretdinov on 09/03/2018.
//  Copyright © 2018 Fix. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ViperMcFlurry/ViperMcFlurry.h>

#import "FPlainFeed.h"

@protocol FFeedDetailsModuleInput <RamblerViperModuleInput>

/**
 @author Oleg Badretdinov

 Метод инициирует стартовую конфигурацию текущего модуля
 */
- (void)configureModule;

@property (nonatomic, retain) FPlainFeed *feed;

@end
