//
//  FFeedDetailsPresenter.m
//  Fix
//
//  Created by Oleg Badretdinov on 09/03/2018.
//  Copyright © 2018 Fix. All rights reserved.
//

#import "FFeedDetailsPresenter.h"

#import "FFeedDetailsViewInput.h"
#import "FFeedDetailsInteractorInput.h"
#import "FFeedDetailsRouterInput.h"

@implementation FFeedDetailsPresenter

@synthesize feed;

#pragma mark - Методы FFeedDetailsModuleInput

- (void)configureModule {
    // Стартовая конфигурация модуля, не привязанная к состоянию view
}

#pragma mark - Методы FFeedDetailsViewOutput

- (void)didTriggerViewReadyEvent {
    if (self.feed) {
        [self.view setFeedText:self.feed.text];
        [self.view setImagesUrls:self.feed.attachements];
        [self.view setLikeText:[NSString stringWithFormat:@"%ld", (long)self.feed.likes.count]];
        [self.view setIsLiked:self.feed.likes.userLikes];
        
        NSDateFormatter *dateFormatter = [NSDateFormatter new];
        dateFormatter.dateStyle = NSDateFormatterLongStyle;
        
        [self.view setDateText:[dateFormatter stringFromDate:self.feed.date]];
    }
}

#pragma mark - Методы FFeedDetailsInteractorOutput

- (void)didTapLikeButton {
    if (self.feed.likes.userLikes) {
        [self.interactor removeLikeFromFeed:self.feed];
    } else {
        [self.interactor likeFeed:feed];
    }
}

- (void)handleError:(NSError *)error {
    [self.view showError:error];
}

- (void)setLike:(BOOL)isLiked {
    [self.view setIsLiked:isLiked];
}

- (void)updateLikeCount:(NSNumber *)like {
    [self.view setLikeText:[NSString stringWithFormat:@"%ld", like.unsignedIntegerValue]];
}

@end
