//
//  FFeedDetailsRouter.h
//  Fix
//
//  Created by Oleg Badretdinov on 09/03/2018.
//  Copyright © 2018 Fix. All rights reserved.
//

#import "FFeedDetailsRouterInput.h"

@protocol RamblerViperModuleTransitionHandlerProtocol;

@interface FFeedDetailsRouter : NSObject <FFeedDetailsRouterInput>

@property (nonatomic, weak) id<RamblerViperModuleTransitionHandlerProtocol> transitionHandler;

@end
