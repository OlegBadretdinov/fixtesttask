//
//  FFeedDetailsViewController.m
//  Fix
//
//  Created by Oleg Badretdinov on 09/03/2018.
//  Copyright © 2018 Fix. All rights reserved.
//

#import "FFeedDetailsViewController.h"
#import "FFeedPhotoCollectionViewCell.h"
#import "FFeedDetailsViewOutput.h"
#import "FPlainFeed.h"
#import "UICollectionViewCell+Identifier.h"
#import <UIImageView+WebCache.h>

@interface FFeedDetailsViewController() <UICollectionViewDataSource, UICollectionViewDelegate>

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UILabel *textLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UIButton *likeButton;
@property (weak, nonatomic) IBOutlet UILabel *likesLabel;

@property (nonatomic, retain) NSArray<FPhoto *>* photos;

@end

@implementation FFeedDetailsViewController

@synthesize feedText, dateText, likeText, isLiked;

#pragma mark - Методы жизненного цикла

- (void)viewDidLoad {
	[super viewDidLoad];

	[self.output didTriggerViewReadyEvent];
}

#pragma mark - Методы FFeedDetailsViewInput

- (void)setFeedText:(NSString *)feedText {
    self.textLabel.text = feedText;
}

- (void)setDateText:(NSString *)dateText {
    self.dateLabel.text = dateText;
}

- (void)setLikeText:(NSString *)likeText {
    self.likesLabel.text = likeText;
}

- (void)setImagesUrls:(NSArray *)urls {
    self.photos = [urls valueForKey:@"photo"];
}

- (void)setIsLiked:(BOOL)isLiked {
    [self.likeButton setSelected:isLiked];
}

- (void)showError:(NSError *)error {
    //TODO: same as FeedList
}

#pragma mark - Other

- (IBAction)handleLikeButtonTap:(id)sender {
    [self.output didTapLikeButton];
}

#pragma mark - UICollectionView

- (nonnull __kindof UICollectionViewCell *)collectionView:(nonnull UICollectionView *)collectionView cellForItemAtIndexPath:(nonnull NSIndexPath *)indexPath {
    FFeedPhotoCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:[FFeedPhotoCollectionViewCell cellIdentifier] forIndexPath:indexPath];
    FPhoto *photo = self.photos[indexPath.row];
    [cell.photoImageView sd_setImageWithURL:photo.photo604];
    return cell;
}

- (NSInteger)collectionView:(nonnull UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.photos.count;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(self.collectionView.frame.size.width/2, self.collectionView.frame.size.height);
}

@end
