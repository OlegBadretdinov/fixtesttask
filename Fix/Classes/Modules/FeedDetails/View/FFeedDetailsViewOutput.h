//
//  FFeedDetailsViewOutput.h
//  Fix
//
//  Created by Oleg Badretdinov on 09/03/2018.
//  Copyright © 2018 Fix. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol FFeedDetailsViewOutput <NSObject>

/**
 @author Oleg Badretdinov

 Метод сообщает презентеру о том, что view готова к работе
 */
- (void)didTriggerViewReadyEvent;
- (void)didTapLikeButton;

@end
