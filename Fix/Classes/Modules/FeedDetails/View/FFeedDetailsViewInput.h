//
//  FFeedDetailsViewInput.h
//  Fix
//
//  Created by Oleg Badretdinov on 09/03/2018.
//  Copyright © 2018 Fix. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol FFeedDetailsViewInput <NSObject>

//Так то можно было бы и объект передавать
@property (nonatomic, retain) NSString* feedText;
@property (nonatomic, retain) NSString* dateText;
@property (nonatomic, retain) NSString* likeText;
@property (nonatomic, assign) BOOL isLiked;

- (void)setImagesUrls:(NSArray *)urls;

- (void)showError:(NSError *)error;

@end
