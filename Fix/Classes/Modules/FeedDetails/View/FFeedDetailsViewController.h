//
//  FFeedDetailsViewController.h
//  Fix
//
//  Created by Oleg Badretdinov on 09/03/2018.
//  Copyright © 2018 Fix. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "FFeedDetailsViewInput.h"

@protocol FFeedDetailsViewOutput;

@interface FFeedDetailsViewController : UIViewController <FFeedDetailsViewInput>

@property (nonatomic, strong) id<FFeedDetailsViewOutput> output;

@end
