//
//  FFeedPhotoCollectionViewCell.m
//  Fix
//
//  Created by Oleg Badretdinov on 10.03.2018.
//  Copyright © 2018 Oleg Badretdinov. All rights reserved.
//

#import "FFeedPhotoCollectionViewCell.h"
#import <UIImageView+WebCache.h>

@implementation FFeedPhotoCollectionViewCell

- (void)prepareForReuse {
    [super prepareForReuse];
    [self.photoImageView sd_cancelCurrentAnimationImagesLoad];
}

@end
