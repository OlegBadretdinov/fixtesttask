//
//  FFeedPhotoCollectionViewCell.h
//  Fix
//
//  Created by Oleg Badretdinov on 10.03.2018.
//  Copyright © 2018 Oleg Badretdinov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FFeedPhotoCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *photoImageView;

@end
