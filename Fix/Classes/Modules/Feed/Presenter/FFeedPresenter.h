//
//  FFeedPresenter.h
//  Fix
//
//  Created by Oleg Badretdinov on 28/02/2018.
//  Copyright © 2018 Fix. All rights reserved.
//

#import "FFeedViewOutput.h"
#import "FFeedInteractorOutput.h"
#import "FFeedModuleInput.h"

@protocol FFeedViewInput;
@protocol FFeedInteractorInput;
@protocol FFeedRouterInput;

@interface FFeedPresenter : NSObject <FFeedModuleInput, FFeedViewOutput, FFeedInteractorOutput>

@property (nonatomic, weak) id<FFeedViewInput> view;
@property (nonatomic, strong) id<FFeedInteractorInput> interactor;
@property (nonatomic, strong) id<FFeedRouterInput> router;

@end
