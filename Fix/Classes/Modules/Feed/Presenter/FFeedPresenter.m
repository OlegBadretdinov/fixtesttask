//
//  FFeedPresenter.m
//  Fix
//
//  Created by Oleg Badretdinov on 28/02/2018.
//  Copyright © 2018 Fix. All rights reserved.
//

#import "FFeedPresenter.h"

#import "FFeedViewInput.h"
#import "FFeedInteractorInput.h"
#import "FFeedRouterInput.h"

@interface FFeedPresenter()

@property (nonatomic, retain) NSMutableArray<FPlainFeed *>* posts;

@end

@implementation FFeedPresenter

- (instancetype)init
{
    self = [super init];
    if (self) {
        _posts = [NSMutableArray new];
    }
    return self;
}

#pragma mark - Методы FFeedModuleInput

- (void)configureModule {
    // Стартовая конфигурация модуля, не привязанная к состоянию view
}

#pragma mark - Методы FFeedViewOutput

- (void)didTriggerViewReadyEvent {
	[self.view setupInitialState];
}

- (void)loadMorePosts {
    [self.interactor loadMorePostsOffset:@(self.posts.count) limit:@25];
}

- (void)refreshFeed {
    [_posts removeAllObjects];
    [self.interactor dropCache];
    [self.interactor loadMorePostsOffset:@(self.posts.count) limit:@25];
}

- (NSArray *)posts {
    return _posts;
}

- (void)didSelectFeed:(FPlainFeed *)feed {
    [self.router showDetailsWithFeed:feed];
}

#pragma mark - Методы FFeedInteractorOutput

- (void)didReceiveError:(NSError *)error {
    [self.view showError:error];
}

- (void)didReceiveNewPosts:(NSArray<FPlainFeed *> *)posts {
    [_posts addObjectsFromArray:posts];
    [self.view updateNewPosts];
}

@end
