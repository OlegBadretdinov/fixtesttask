//
//  FFeedModuleInput.h
//  Fix
//
//  Created by Oleg Badretdinov on 28/02/2018.
//  Copyright © 2018 Fix. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ViperMcFlurry/ViperMcFlurry.h>

@protocol FFeedModuleInput <RamblerViperModuleInput>

/**
 @author Oleg Badretdinov

 Метод инициирует стартовую конфигурацию текущего модуля
 */
- (void)configureModule;

@end
