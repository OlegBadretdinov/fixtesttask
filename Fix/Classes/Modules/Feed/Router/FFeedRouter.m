//
//  FFeedRouter.m
//  Fix
//
//  Created by Oleg Badretdinov on 28/02/2018.
//  Copyright © 2018 Fix. All rights reserved.
//

#import "FFeedRouter.h"
#import "FFeedDetailsModuleInput.h"

#import <ViperMcFlurry/ViperMcFlurry.h>

@implementation FFeedRouter

- (void)showDetailsWithFeed:(FPlainFeed *)feed {
    [[self.transitionHandler openModuleUsingSegue:@"FeedDetails"] thenChainUsingBlock:^id<RamblerViperModuleOutput>(id<FFeedDetailsModuleInput> moduleInput) {
        moduleInput.feed = feed;
        return nil;
    }];;
}

#pragma mark - Методы FFeedRouterInput

@end
