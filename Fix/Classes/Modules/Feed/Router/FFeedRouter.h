//
//  FFeedRouter.h
//  Fix
//
//  Created by Oleg Badretdinov on 28/02/2018.
//  Copyright © 2018 Fix. All rights reserved.
//

#import "FFeedRouterInput.h"

@protocol RamblerViperModuleTransitionHandlerProtocol;

@interface FFeedRouter : NSObject <FFeedRouterInput>

@property (nonatomic, weak) id<RamblerViperModuleTransitionHandlerProtocol> transitionHandler;

@end
