//
//  FFeedViewOutput.h
//  Fix
//
//  Created by Oleg Badretdinov on 28/02/2018.
//  Copyright © 2018 Fix. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "FPlainFeed.h"

@protocol FFeedViewOutput <NSObject>

/**
 @author Oleg Badretdinov

 Метод сообщает презентеру о том, что view готова к работе
 */
- (void)didTriggerViewReadyEvent;

- (void)didSelectFeed:(FPlainFeed *)feed;

- (void)loadMorePosts;
- (void)refreshFeed;
- (NSArray *)posts;

@end
