//
//  FFeedLoaderTableViewCell.m
//  Fix
//
//  Created by Oleg Badretdinov on 04.03.2018.
//  Copyright © 2018 Oleg Badretdinov. All rights reserved.
//

#import "FFeedLoaderTableViewCell.h"

@interface FFeedLoaderTableViewCell()

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@end

@implementation FFeedLoaderTableViewCell

- (void)prepareForReuse {
    [super prepareForReuse];
    
    [self.activityIndicator startAnimating];
}

@end
