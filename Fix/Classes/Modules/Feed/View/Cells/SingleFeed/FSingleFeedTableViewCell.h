//
//  FSingleFeedTableViewCell.h
//  Fix
//
//  Created by Oleg Badretdinov on 04.03.2018.
//  Copyright © 2018 Oleg Badretdinov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FSingleFeedTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *likeCount;
@property (weak, nonatomic) IBOutlet UILabel *commentsCount;
@property (weak, nonatomic) IBOutlet UILabel *repostCount;
@property (weak, nonatomic) IBOutlet UIImageView *firstImage;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;

- (void)hideImageView;
- (void)showImageView;

@end
