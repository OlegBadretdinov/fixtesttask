//
//  FSingleFeedTableViewCell.m
//  Fix
//
//  Created by Oleg Badretdinov on 04.03.2018.
//  Copyright © 2018 Oleg Badretdinov. All rights reserved.
//

#import "FSingleFeedTableViewCell.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface FSingleFeedTableViewCell()

@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imageHeightConstraint;

@end

@implementation FSingleFeedTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self setupContainer];
}

- (void)setupContainer {
    self.containerView.layer.cornerRadius = 4;
    self.containerView.layer.shadowRadius = 5;
    self.containerView.layer.shadowOpacity = 0.16;
    self.containerView.layer.shadowColor = [UIColor blackColor].CGColor;
    self.containerView.layer.shadowOffset = CGSizeMake(0, 3);
    self.containerView.layer.shouldRasterize = YES;
    self.containerView.layer.rasterizationScale = [UIScreen mainScreen].scale;
}

- (void)prepareForReuse {
    [super prepareForReuse];
    
    [self showImageView];
    self.firstImage.image = nil;
    [self.firstImage sd_cancelCurrentAnimationImagesLoad];
    
    self.textLabel.text = nil;
}

- (void)hideImageView {
    self.imageHeightConstraint.constant = 0;
}

- (void)showImageView {
    self.imageHeightConstraint.constant = 100;
}

@end
