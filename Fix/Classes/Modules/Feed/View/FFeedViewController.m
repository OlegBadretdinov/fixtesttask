//
//  FFeedViewController.m
//  Fix
//
//  Created by Oleg Badretdinov on 28/02/2018.
//  Copyright © 2018 Fix. All rights reserved.
//

#import "FFeedViewController.h"
#import "FFeedLoaderTableViewCell.h"
#import "UITableViewCell+Identifier.h"
#import "FSingleFeedTableViewCell.h"
#import "FFeedViewOutput.h"

#import <SDWebImage/UIImageView+WebCache.h>

@interface FFeedViewController() <UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, retain) UIRefreshControl *refreshControl;

@end

@implementation FFeedViewController

#pragma mark - Методы жизненного цикла

- (void)viewDidLoad {
	[super viewDidLoad];
    [self setupTableView];
	[self.output didTriggerViewReadyEvent];
}

#pragma mark - Методы FFeedViewInput

- (void)setupInitialState {
	// В этом методе происходит настройка параметров view, зависящих от ее жизненого цикла (создание элементов, анимации и пр.)
}

- (void)updateNewPosts {
    [self.refreshControl endRefreshing];
    [self.tableView reloadData]; //В идеале сделать нормальное обновление экрана
}

- (void)showError:(NSError *)error {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:error.localizedDescription preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Ок" style:UIAlertActionStyleDefault handler:nil];
    [alert addAction:cancel];
    [self presentViewController:alert animated:YES completion:nil];
}


#pragma mark - Private

- (void)setupTableView {
    [self.tableView registerNib:[UINib nibWithNibName:[FFeedLoaderTableViewCell cellIdentifier]  bundle:nil] forCellReuseIdentifier:[FFeedLoaderTableViewCell cellIdentifier]];
    [self.tableView registerNib:[UINib nibWithNibName:[FSingleFeedTableViewCell cellIdentifier]  bundle:nil] forCellReuseIdentifier:[FSingleFeedTableViewCell cellIdentifier]];
    
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 300;
    
    self.refreshControl = [UIRefreshControl new];
    [self.refreshControl addTarget:self action:@selector(handlePullToRefresh) forControlEvents:UIControlEventValueChanged];
    
    if (@available(iOS 10.0, *)) {
        self.tableView.refreshControl = self.refreshControl;
    } else {
        [self.tableView addSubview:self.refreshControl];
    }
}

- (void)handlePullToRefresh {
    [self.output refreshFeed];
}

#pragma mark - UITableViewDataSource

- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    if (indexPath.row < self.output.posts.count) {
        FSingleFeedTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[FSingleFeedTableViewCell cellIdentifier]];
        
        FPlainFeed *post = self.output.posts[indexPath.row];
        
        cell.likeCount.text = [NSString stringWithFormat:@"%ld",(long)post.likes.count];
        cell.commentsCount.text = [NSString stringWithFormat:@"%ld",(long)post.comments.count];
        cell.repostCount.text = [NSString stringWithFormat:@"%ld",(long)post.reposts.count];
        
        if (post.attachements.count > 0) {
            [cell.firstImage sd_setImageWithURL:post.attachements.firstObject.photo.photo604];
        } else {
            [cell hideImageView];
        }
        cell.descriptionLabel.text = post.text;
        
        return cell;
    } else {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[FFeedLoaderTableViewCell cellIdentifier]];
        
        return cell;
    }
}

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.output.posts.count + 1;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row >= self.output.posts.count) {
        [self.output loadMorePosts];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row < self.output.posts.count) {
        FPlainFeed *feed = self.output.posts[indexPath.row];
        [self.output didSelectFeed:feed];
    }
}

@end
