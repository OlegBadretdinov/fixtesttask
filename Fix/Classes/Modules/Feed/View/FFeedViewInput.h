//
//  FFeedViewInput.h
//  Fix
//
//  Created by Oleg Badretdinov on 28/02/2018.
//  Copyright © 2018 Fix. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FPlainFeed.h"

@protocol FFeedViewInput <NSObject>

/**
 @author Oleg Badretdinov

 Метод настраивает начальный стейт view
 */
- (void)setupInitialState;

- (void)updateNewPosts;

- (void)showError:(NSError *)error;

@end
