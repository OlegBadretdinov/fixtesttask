//
//  FFeedViewController.h
//  Fix
//
//  Created by Oleg Badretdinov on 28/02/2018.
//  Copyright © 2018 Fix. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "FFeedViewInput.h"

@protocol FFeedViewOutput;

@interface FFeedViewController : UIViewController <FFeedViewInput>

@property (nonatomic, strong) id<FFeedViewOutput> output;

@end
