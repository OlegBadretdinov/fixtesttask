//
//  FFeedAssembly.h
//  Fix
//
//  Created by Oleg Badretdinov on 28/02/2018.
//  Copyright © 2018 Fix. All rights reserved.
//

#import <Typhoon/Typhoon.h>
#import <RamblerTyphoonUtils/AssemblyCollector.h>

/**
 @author Oleg Badretdinov

 Feed module
 */
@interface FFeedAssembly : TyphoonAssembly <RamblerInitialAssembly>

@end
