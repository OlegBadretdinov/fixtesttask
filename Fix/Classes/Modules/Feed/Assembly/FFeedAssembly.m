//
//  FFeedAssembly.m
//  Fix
//
//  Created by Oleg Badretdinov on 28/02/2018.
//  Copyright © 2018 Fix. All rights reserved.
//

#import "FFeedAssembly.h"

#import "FFeedViewController.h"
#import "FFeedInteractor.h"
#import "FFeedPresenter.h"
#import "FFeedRouter.h"
#import "FFeedService.h"
#import "FApplicationAssembly.h"

#import <ViperMcFlurry/ViperMcFlurry.h>

@implementation FFeedAssembly

- (FFeedService *)feedService {
    return [TyphoonDefinition withClass:[FFeedService class]];
}

- (FFeedViewController *)viewFeed {
    return [TyphoonDefinition withClass:[FFeedViewController class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterFeed]];
                              [definition injectProperty:@selector(moduleInput)
                                                    with:[self presenterFeed]];
                          }];
}

- (FFeedInteractor *)interactorFeed {
    return [TyphoonDefinition withClass:[FFeedInteractor class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterFeed]];
                              [definition injectProperty:@selector(feedService)
                                                    with:[self feedService]];
                          }];
}

- (FFeedPresenter *)presenterFeed{
    return [TyphoonDefinition withClass:[FFeedPresenter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(view)
                                                    with:[self viewFeed]];
                              [definition injectProperty:@selector(interactor)
                                                    with:[self interactorFeed]];
                              [definition injectProperty:@selector(router)
                                                    with:[self routerFeed]];
                          }];
}

- (FFeedRouter *)routerFeed{
    return [TyphoonDefinition withClass:[FFeedRouter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(transitionHandler)
                                                    with:[self viewFeed]];
                          }];
}

@end
