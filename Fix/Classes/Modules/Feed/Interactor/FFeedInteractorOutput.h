//
//  FFeedInteractorOutput.h
//  Fix
//
//  Created by Oleg Badretdinov on 28/02/2018.
//  Copyright © 2018 Fix. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FPlainFeed.h"

@protocol FFeedInteractorOutput <NSObject>

- (void)didReceiveNewPosts:(NSArray<FPlainFeed *>*)posts;
- (void)didReceiveError:(NSError *)error;

@end
