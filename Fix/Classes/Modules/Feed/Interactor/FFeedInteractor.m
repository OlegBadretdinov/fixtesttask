//
//  FFeedInteractor.m
//  Fix
//
//  Created by Oleg Badretdinov on 28/02/2018.
//  Copyright © 2018 Fix. All rights reserved.
//

#import "FFeedInteractor.h"
#import "FFeedService.h"
#import <extobjc.h>

#import "FFeedInteractorOutput.h"

@implementation FFeedInteractor

#pragma mark - Методы FFeedInteractorInput

- (void)loadMorePostsOffset:(NSNumber *)offset limit:(NSNumber *)limit {
    [self.feedService getFeedWithOffset:offset count:limit completionBlock:^(NSArray<FPlainFeed *> *feedArray, NSError *error) {
        if (feedArray) {
            [self.output didReceiveNewPosts:feedArray];
        } else {
            [self.output didReceiveError:error];
        }
    }];
}

- (void)dropCache {
    [self.feedService clearFeedCacheWithCompletionBlock:nil];
}

@end
