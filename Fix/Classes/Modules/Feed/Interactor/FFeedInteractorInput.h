//
//  FFeedInteractorInput.h
//  Fix
//
//  Created by Oleg Badretdinov on 28/02/2018.
//  Copyright © 2018 Fix. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FPlainFeed.h"

@protocol FFeedInteractorInput <NSObject>

- (void)dropCache;
- (void)loadMorePostsOffset:(NSNumber *)offset limit:(NSNumber *)limit;

@end
