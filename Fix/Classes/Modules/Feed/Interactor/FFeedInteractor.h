//
//  FFeedInteractor.h
//  Fix
//
//  Created by Oleg Badretdinov on 28/02/2018.
//  Copyright © 2018 Fix. All rights reserved.
//

#import "FFeedInteractorInput.h"

@protocol FFeedInteractorOutput;
@protocol FFeedServiceProtocol;

@interface FFeedInteractor : NSObject <FFeedInteractorInput>

@property (nonatomic, retain) id<FFeedServiceProtocol> feedService;
@property (nonatomic, weak) id<FFeedInteractorOutput> output;

@end
