//
//  FAuthorizationPresenter.h
//  Fix
//
//  Created by Oleg Badretdinov on 26/02/2018.
//  Copyright © 2018 Fix. All rights reserved.
//

#import "FAuthorizationViewOutput.h"
#import "FAuthorizationInteractorOutput.h"
#import "FAuthorizationModuleInput.h"
#import "FAuthorizationTimerService.h"

@protocol FAuthorizationViewInput;
@protocol FAuthorizationInteractorInput;
@protocol FAuthorizationRouterInput;

@interface FAuthorizationPresenter : NSObject <FAuthorizationModuleInput, FAuthorizationViewOutput, FAuthorizationInteractorOutput>

@property (nonatomic, retain) id<FAuthorizationTimerServiceProtocol> authTimer;

@property (nonatomic, weak) id<FAuthorizationViewInput> view;
@property (nonatomic, strong) id<FAuthorizationInteractorInput> interactor;
@property (nonatomic, strong) id<FAuthorizationRouterInput> router;

@end
