//
//  FAuthorizationPresenter.m
//  Fix
//
//  Created by Oleg Badretdinov on 26/02/2018.
//  Copyright © 2018 Fix. All rights reserved.
//

#import "FAuthorizationPresenter.h"

#import "FAuthorizationViewInput.h"
#import "FAuthorizationInteractorInput.h"
#import "FAuthorizationRouterInput.h"

#import <Typhoon.h>

@interface FAuthorizationPresenter () <FAuthorizationTimerServiceDelegate>

@end

@implementation FAuthorizationPresenter

- (void)typhoonDidInject {
    self.authTimer.delegate = self;
}

#pragma mark - Методы FAuthorizationModuleInput

- (void)configureModule {
    // Стартовая конфигурация модуля, не привязанная к состоянию view
}

#pragma mark - Методы FAuthorizationViewOutput

- (void)didTriggerViewReadyEvent {
	[self.view setupInitialState];
    [self.interactor checkAuthorizationStatus];
}

- (void)handleAuthButtonTap {
    [self.view disableAuthButton];
    [self.interactor startAuthorization];
}

- (void)handleLogOutButtonTap {
    [self.interactor logOut];
}

#pragma mark - Методы FAuthorizationInteractorOutput

- (void)authorizationDidFailed {
    [self.view enableAuthButton];
    [self.view stopActivityIndicator];
    [self.authTimer stopTimer];
    [self.view showAuthError];
}

- (void)authorizationSuccess {
    [self.view enableAuthButton];
    [self.view stopActivityIndicator];
    [self.authTimer stopTimer];
    [self.view showSuccessAuth];
    [self.router presentFeed];
}

- (void)authorizationWaiting {
    [self.view startActivityIndicator];
    [self.authTimer startTimer];
}

- (void)authorizationReady {
    [self.view enableAuthButton];
    [self.view stopActivityIndicator];
}

#pragma mark - FAuthorizationTimerServiceDelegate

- (void)shouldCheckAuthorizationStatus {
    [self.interactor checkAuthorizationStatus];
}

@end
