//
//  FAuthorizationInteractor.m
//  Fix
//
//  Created by Oleg Badretdinov on 26/02/2018.
//  Copyright © 2018 Fix. All rights reserved.
//

#import "FAuthorizationInteractor.h"

#import "FAuthorizationInteractorOutput.h"
#import <Typhoon.h>

@interface FAuthorizationInteractor() <FAuthorizationServiceDelegate>

@end

@implementation FAuthorizationInteractor

- (void)typhoonDidInject {
    self.authorizationService.delegate = self;
}

#pragma mark - Методы FAuthorizationInteractorInput

- (void)startAuthorization {
    [self.authorizationService startAuthorization];
}

- (void)checkAuthorizationStatus {
    [self.authorizationService checkAuthorizationStatus];
}

- (void)logOut {
    [self.authorizationService logOut];
}

#pragma mark - AuthorizationServiceDelegate

-(void)authDidChangeStatus:(FAuthorizationStatus)status {
    switch (status) {
        case FAuthorizationStatusWaiting:
            [self.output authorizationWaiting];
            break;
        case FAuthorizationStatusError:
            [self.output authorizationDidFailed];
            break;
        case FAuthorizationStatusAuthorized:
            [self.output authorizationSuccess];
            break;
        case FAuthorizationStatusUnknown:
            [self.output authorizationReady];
            break;
        default:
            break;
    }
}

@end
