//
//  FAuthorizationInteractor.h
//  Fix
//
//  Created by Oleg Badretdinov on 26/02/2018.
//  Copyright © 2018 Fix. All rights reserved.
//

#import "FAuthorizationInteractorInput.h"
#import "FAuthorizationService.h"

@protocol FAuthorizationInteractorOutput;

@interface FAuthorizationInteractor : NSObject <FAuthorizationInteractorInput>

@property (nonatomic, weak) id<FAuthorizationInteractorOutput> output;
@property (nonatomic, retain) id<FAuthorizationServiceProtocol> authorizationService;

@end
