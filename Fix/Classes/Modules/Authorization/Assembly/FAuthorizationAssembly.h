//
//  FAuthorizationAssembly.h
//  Fix
//
//  Created by Oleg Badretdinov on 26/02/2018.
//  Copyright © 2018 Fix. All rights reserved.
//

#import <Typhoon/Typhoon.h>
#import <RamblerTyphoonUtils/AssemblyCollector.h>

/**
 @author Oleg Badretdinov

 Authorization module
 */
@interface FAuthorizationAssembly : TyphoonAssembly <RamblerInitialAssembly>

@end
