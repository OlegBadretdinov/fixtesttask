//
//  FAuthorizationAssembly.m
//  Fix
//
//  Created by Oleg Badretdinov on 26/02/2018.
//  Copyright © 2018 Fix. All rights reserved.
//

#import "FAuthorizationAssembly.h"

#import "FAuthorizationViewController.h"
#import "FAuthorizationInteractor.h"
#import "FAuthorizationPresenter.h"
#import "FAuthorizationRouter.h"

#import <ViperMcFlurry/ViperMcFlurry.h>

@implementation FAuthorizationAssembly

- (FAuthorizationTimerService *)authorizationTimerService {
    return [TyphoonDefinition withClass:[FAuthorizationTimerService class]];
}

- (FAuthorizationService *)authorizationService {
    return [TyphoonDefinition withClass:[FAuthorizationService class]];
}

- (FAuthorizationViewController *)viewAuthorization {
    return [TyphoonDefinition withClass:[FAuthorizationViewController class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterAuthorization]];
                              [definition injectProperty:@selector(moduleInput)
                                                    with:[self presenterAuthorization]];
                          }];
}

- (FAuthorizationInteractor *)interactorAuthorization {
    return [TyphoonDefinition withClass:[FAuthorizationInteractor class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterAuthorization]];
                              [definition injectProperty:@selector(authorizationService)
                                                    with:[self authorizationService]];
                          }];
}

- (FAuthorizationPresenter *)presenterAuthorization{
    return [TyphoonDefinition withClass:[FAuthorizationPresenter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(view)
                                                    with:[self viewAuthorization]];
                              [definition injectProperty:@selector(interactor)
                                                    with:[self interactorAuthorization]];
                              [definition injectProperty:@selector(router)
                                                    with:[self routerAuthorization]];
                              [definition injectProperty:@selector(authTimer)
                                                    with:[self authorizationTimerService]];
                          }];
}

- (FAuthorizationRouter *)routerAuthorization{
    return [TyphoonDefinition withClass:[FAuthorizationRouter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(transitionHandler)
                                                    with:[self viewAuthorization]];
                          }];
}

@end
