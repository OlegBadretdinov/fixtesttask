//
//  FAuthorizationRouter.h
//  Fix
//
//  Created by Oleg Badretdinov on 26/02/2018.
//  Copyright © 2018 Fix. All rights reserved.
//

#import "FAuthorizationRouterInput.h"

@protocol RamblerViperModuleTransitionHandlerProtocol;

@interface FAuthorizationRouter : NSObject <FAuthorizationRouterInput>

@property (nonatomic, weak) id<RamblerViperModuleTransitionHandlerProtocol> transitionHandler;

@end
