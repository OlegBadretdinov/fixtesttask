//
//  FAuthorizationRouterInput.h
//  Fix
//
//  Created by Oleg Badretdinov on 26/02/2018.
//  Copyright © 2018 Fix. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol FAuthorizationRouterInput <NSObject>

- (void)presentFeed;

@end
