//
//  FAuthorizationRouter.m
//  Fix
//
//  Created by Oleg Badretdinov on 26/02/2018.
//  Copyright © 2018 Fix. All rights reserved.
//

#import "FAuthorizationRouter.h"

#import <ViperMcFlurry/ViperMcFlurry.h>

@implementation FAuthorizationRouter

#pragma mark - Методы FAuthorizationRouterInput

- (void)presentFeed {
    [self.transitionHandler openModuleUsingSegue:@"FeedSegue"];
}

@end
