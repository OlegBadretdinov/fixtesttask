//
//  FAuthorizationViewOutput.h
//  Fix
//
//  Created by Oleg Badretdinov on 26/02/2018.
//  Copyright © 2018 Fix. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol FAuthorizationViewOutput <NSObject>

/**
 @author Oleg Badretdinov

 Метод сообщает презентеру о том, что view готова к работе
 */
- (void)didTriggerViewReadyEvent;

- (void)handleAuthButtonTap;

- (void)handleLogOutButtonTap;

@end
