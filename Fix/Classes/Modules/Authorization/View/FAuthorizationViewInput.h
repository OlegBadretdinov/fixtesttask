//
//  FAuthorizationViewInput.h
//  Fix
//
//  Created by Oleg Badretdinov on 26/02/2018.
//  Copyright © 2018 Fix. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol FAuthorizationViewInput <NSObject>

/**
 @author Oleg Badretdinov

 Метод настраивает начальный стейт view
 */
- (void)setupInitialState;

- (void)disableAuthButton;
- (void)enableAuthButton;

- (void)startActivityIndicator;
- (void)stopActivityIndicator;

- (void)showAuthError;
- (void)showSuccessAuth;

@end
