//
//  FAuthorizationViewController.h
//  Fix
//
//  Created by Oleg Badretdinov on 26/02/2018.
//  Copyright © 2018 Fix. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "FAuthorizationViewInput.h"

@protocol FAuthorizationViewOutput;

@interface FAuthorizationViewController : UIViewController <FAuthorizationViewInput>

@property (nonatomic, strong) id<FAuthorizationViewOutput> output;

-(IBAction)logOutUnwindSegue:(UIStoryboardSegue *)segue;

@end
