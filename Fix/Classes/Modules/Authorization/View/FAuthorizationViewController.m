//
//  FAuthorizationViewController.m
//  Fix
//
//  Created by Oleg Badretdinov on 26/02/2018.
//  Copyright © 2018 Fix. All rights reserved.
//

#import "FAuthorizationViewController.h"

#import "FAuthorizationViewOutput.h"

@interface FAuthorizationViewController()

@property (weak, nonatomic) IBOutlet UIButton *authButton;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@end

@implementation FAuthorizationViewController

#pragma mark - Методы жизненного цикла

- (void)viewDidLoad {
	[super viewDidLoad];

	[self.output didTriggerViewReadyEvent];
}

#pragma mark - Методы FAuthorizationViewInput

- (void)setupInitialState {
    [self disableAuthButton];
    [self startActivityIndicator];
}

- (void)disableAuthButton {
    [self.authButton setEnabled:NO];
}
- (void)enableAuthButton {
    [self.authButton setEnabled:YES];
}

- (void)startActivityIndicator {
    [self.activityIndicator startAnimating];
}
- (void)stopActivityIndicator {
    [self.activityIndicator stopAnimating];
}

- (void)showAuthError {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Ошибка" message:@"Попробуйте позже" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *action = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    [alert addAction:action];
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)showSuccessAuth {
    
}

#pragma mark - IBActions

- (IBAction)handleAuthButtonTap:(id)sender {
    [self.output handleAuthButtonTap];
}

-(IBAction)logOutUnwindSegue:(UIStoryboardSegue *)segue {
    [self.output handleLogOutButtonTap];
}

@end
