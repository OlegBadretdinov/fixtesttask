//
//  AppDelegate.h
//  Fix
//
//  Created by Лена Вышутина on 26.02.2018.
//  Copyright © 2018 Oleg Badretdinov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

